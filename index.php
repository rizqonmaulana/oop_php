<?php

require_once 'Animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

$sheep = new Animal("shaun");

echo $sheep->getName()."<br>";
echo $sheep->getLegs()."<br>";
var_dump($sheep->getColdBlooded());
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo $sungokong->getName()."<br>";
echo $sungokong->getLegs()."<br>";
var_dump($sungokong->getColdBlooded());
echo "<br>";
$sungokong->yell()."<br>"; // "Auooo"
echo "<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo $kodok->getName()."<br>";
echo $kodok->getLegs()."<br>";
var_dump($kodok->getColdBlooded());
echo "<br>";
$kodok->jump()."<br>"; // "hop hop"
