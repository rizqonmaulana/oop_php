<?php

class Animal {
    protected $name,
              $legs,
              $cold_blooded;

    public function __construct($name="shaun", $legs=2, $cold_blooded=false){
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }

    public function getName(){
        return $this->name;
    }

    public function getLegs(){
        return $this->legs;
    }

    public function getColdBlooded(){
        return $this->cold_blooded;
    }
}