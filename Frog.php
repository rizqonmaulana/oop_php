<?php
require_once 'Animal.php';

class Frog extends Animal {
    public function __construct($name="", $legs=4, $cold_blooded=false){
        parent::__construct($name, $legs, $cold_blooded);
    }    

    public function jump(){
        echo "hop hop";
    }
}